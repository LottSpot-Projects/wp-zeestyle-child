<?php

if ($_GET['viewall']) {

	function all_pages( $content ) {

		if ( is_single() && $GLOBALS['multipage'] ) {
			$content = '<div class="page">' . join('</div><div class="page">', $GLOBALS['pages']) . '</div>';
		}

		return $content;

	}

	add_filter( 'the_content', 'all_pages' );

}

function view_all_or_selected() {

	$link = NULL;
	if(!$_GET['viewall']) { 
		$link = sprintf( '<a href="%s">View all</a>', add_query_arg(array('viewall' => 1)) );
	} else {
		$link = sprintf( '<a href="%s">Back to selected page</a>', remove_query_arg( 'viewall' ) );
	}

	return $link;

}

function add_viewall_link( $args ) {
	$args['before'] = '<p>' . view_all_or_selected();
	
	return $args;
}
add_filter( 'wp_link_pages_args', add_viewall_link, 1 );

?>
